import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println(String.format("Fruits in stock: %s", Arrays.toString(fruits)));
        System.out.println("Which fruit would you like to get the index of?");
        String fruitSearch = scanner.nextLine();
        int result = Arrays.binarySearch(fruits, fruitSearch);
        System.out.println(String.format("The index of %s is %s", fruitSearch, result));

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Alice", "Bob", "Carol", "Dennis"));

        System.out.println(String.format("My friends are: %s", friends));

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println(String.format("Our inventory consists of: \n%s", inventory));
    }
}